package hzt.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BoidTest {

    @Test
    void testInitConnectionsCountIsZero() {
        assertEquals(0, new Boid().getAmountOfConnections());
    }
}
