package hzt.controller;

import java.awt.*;

public abstract class AppConstants {
    // constants
    public static final Dimension SCREEN_SIZE = Toolkit.getDefaultToolkit().getScreenSize();
    public static final Dimension INIT_SCENE_SIZE =  new Dimension(600, 600);
//    public static final Dimension INIT_SCENE_SIZE =  new Dimension(SCREEN_SIZE.width * 7 / 12,
//            SCREEN_SIZE.height * 5 / 12);
    public static final int FONT_SIZE_ANNOUNCEMENTS = 80;
    public static final double INIT_FRAME_RATE = 25; // f/s
    public static final String TITLE = "Graph Visualizer";

    // http://patorFX.com/software/taag/#p=display&f=Big&t=A%20%20

    private static final String ANSI_RESET = "\u001B[0m";
    private static final String ANSI_RED = "\u001B[31m";
    private static final String ANSI_GREEN = "\u001B[32m";
    private static final String ANSI_YELLOW = "\u001B[33m";
    private static final String ANSI_BLUE = "\u001B[34m";
    private static final String ANSI_YELLOW_BACKGROUND = "\u001B[43m";
    private static final String ANSI_PURPLE = "\u001B[35m";
    private static final String ANSI_CYAN = "\u001B[36m";
    private static final String ANSI_WHITE = "\u001B[37m";
    private static final String ANSI_BRIGHT_BLACK = "\u001B[90m";
    private static final String ANSI_BRIGHT_RED = "\u001B[91m";
    private static final String ANSI_BRIGHT_GREEN = "\u001B[92m";
    private static final String ANSI_BRIGHT_YELLOW = "\u001B[93m";
    private static final String ANSI_BRIGHT_BLUE = "\u001B[94m";
    private static final String ANSI_BRIGHT_PURPLE = "\u001B[95m";
    private static final String ANSI_BRIGHT_CYAN = "\u001B[96m";
    private static final String ANSI_BRIGHT_WHITE = "\u001B[97m";
    private static final String ANSI_BLACK_BACKGROUND = "\u001B[40m";
    private static final String ANSI_RED_BACKGROUND = "\u001B[41m";
    private static final String ANSI_GREEN_BACKGROUND = "\u001B[42m";
    private static final String ANSI_BLACK = "\u001B[30m";
    private static final String ANSI_BLUE_BACKGROUND = "\u001B[44m";
    private static final String ANSI_PURPLE_BACKGROUND = "\u001B[45m";
    private static final String ANSI_CYAN_BACKGROUND = "\u001B[46m";
    private static final String ANSI_WHITE_BACKGROUND = "\u001B[47m";
    private static final String ANSI_BRIGHT_BG_BLACK = "\u001B[100m";
    private static final String ANSI_BRIGHT_BG_RED = "\u001B[101m";
    private static final String ANSI_BRIGHT_BG_GREEN = "\u001B[102m";
    private static final String ANSI_BRIGHT_BG_YELLOW = "\u001B[103m";
    private static final String ANSI_BRIGHT_BG_BLUE = "\u001B[104m";
    private static final String ANSI_BRIGHT_BG_PURPLE = "\u001B[105m";
    private static final String ANSI_BRIGHT_BG_CYAN = "\u001B[106m";
    private static final String ANSI_BRIGHT_BG_WHITE = "\u001B[107m";

    public static final String DOTTED_LINE = "----------------------------------------------------------------------------------------\n";

    public static final String TITLE_FX = ANSI_BLUE +
            TITLE + ANSI_RESET;

    public static final String LOOK_IN_OUTPUT = ANSI_GREEN +
            "  _                 _      _         _   _                        _               _      __      _     _                            \n" +
            " | |               | |    (_)       | | | |                      | |             | |    / _|    | |   | |                           \n" +
            " | |     ___   ___ | | __  _ _ __   | |_| |__   ___    ___  _   _| |_ _ __  _   _| |_  | |_ ___ | | __| | ___ _ __                  \n" +
            " | |    / _ \\ / _ \\| |/ / | | '_ \\  | __| '_ \\ / _ \\  / _ \\| | | | __| '_ \\| | | | __| |  _/ _ \\| |/ _` |/ _ \\ '__|        \n" +
            " | |___| (_) | (_) |   <  | | | | | | |_| | | |  __/ | (_) | |_| | |_| |_) | |_| | |_  | || (_) | | (_| |  __/ |_ _ _               \n" +
            " |______\\___/ \\___/|_|\\_\\ |_|_| |_|  \\__|_| |_|\\___|  \\___/ \\__,_|\\__| .__/ \\__,_|\\__| |_| \\___/|_|\\__,_|\\___|_(_|_|_)\n" +
            "                                                                     | |                                                            \n" +
            "                                                                     |_|                                                            \n" + ANSI_RESET;

    public static final String CLOSING_MESSAGE = ANSI_BLUE +
            "See you next Time! :)" +
            ANSI_RESET;

    private static final String BYE = ANSI_YELLOW +
            "  ____               _   \n" +
            " |  _ \\             | | \n" +
            " | |_) |_   _  ___  | |  \n" +
            " |  _ <| | | |/ _ \\ | | \n" +
            " | |_) | |_| |  __/ |_|  \n" +
            " |____/ \\__, |\\___| (_)\n" +
            "         __/ |           \n" +
            "        |___/            \n" + ANSI_RESET;
}
