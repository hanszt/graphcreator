package hzt.controller;

import javafx.animation.Timeline;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;

public class AppVars {

    long startTimeSim;
    long runTimeSim;
    long tStart;
    long tStop;

    Scene scene;
    StackPane root;
    Timeline timeline;
    GridPane grid;
    Canvas canvas;

    public Scene getScene() {
        return scene;
    }

    public StackPane getRoot() {
        return root;
    }

    public double getRunTimeSim() {
        return runTimeSim;
    }

    public Timeline getTimeline() {
        return timeline;
    }
}
