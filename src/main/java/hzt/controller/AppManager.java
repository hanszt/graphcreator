package hzt.controller;

import hzt.view.ColorController;
import hzt.view.DrawAnimation;
import hzt.view.StatsController;
import hzt.view.UserInputController;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static hzt.controller.AppConstants.*;
import static hzt.view.ColorController.getBackgroundColor;
import static hzt.view.ColorController.resetColors;
import static java.lang.String.format;
import static java.lang.System.nanoTime;
import static javafx.animation.Animation.Status.PAUSED;
import static javafx.util.Duration.seconds;

public class AppManager extends AppVars {

    private static final Logger LOGGER = LoggerFactory.getLogger(AppManager.class);

    private static int instances = 0;
    private final int instance = ++instances;

    private final UserInputController uc = new UserInputController(this);
    private final DrawAnimation a = new DrawAnimation(this, uc);
    private final StatsController s = new StatsController(this, a);

    public void setupStage(Stage stage) {
        final var startingText = "%s%nAnimation of instance %d starting...".formatted(TITLE_FX, instance);
        LOGGER.info(startingText);
        setupScene();
        stage.setScene(scene);
        stage.setMinWidth(500);
        stage.setMinHeight(500);
        stage.setTitle(format("%s (%d)", TITLE, instance));
        stage.setOnCloseRequest(e -> printClosingText());
        start();
        stage.show();
        runTimeSim = nanoTime();
    }

    private void setupScene() {
        root = new StackPane();
        root.setBackground(new Background(new BackgroundFill(getBackgroundColor(), CornerRadii.EMPTY, Insets.EMPTY)));
        scene = new Scene(root, INIT_SCENE_SIZE.width, INIT_SCENE_SIZE.height);
        root.setAlignment(Pos.CENTER);
        canvas = new Canvas();
        canvas.widthProperty().bind(scene.widthProperty());
        canvas.heightProperty().bind(scene.heightProperty());
        timeline = new Timeline(new KeyFrame(seconds(1 / INIT_FRAME_RATE), event -> manageSim(canvas.getGraphicsContext2D())));
        timeline.setCycleCount(Animation.INDEFINITE);
        grid = uc.setupControls(a);
        root.getChildren().addAll(canvas, grid);
    }

    private void setupStartScreen(GraphicsContext gc) {
        gc.setStroke(ColorController.getTextColor());
        gc.setLineWidth(1);
        gc.setTextAlign(TextAlignment.CENTER);
        gc.setFont(new Font("", FONT_SIZE_ANNOUNCEMENTS));
        gc.strokeText(TITLE, scene.getWidth() * 3 / 5, scene.getHeight() / 2.);
    }

    private void start() {
        startTimeSim = nanoTime();
        tStart = nanoTime();
        setupStartScreen(canvas.getGraphicsContext2D());
        timeline.play();
    }

    private void manageSim(GraphicsContext graphics) {
        if (nanoTime() - tStart > 1.5e9) {
            if (startTimeSim == 0L) {
                startTimeSim = nanoTime();
                graphics.clearRect(0, 0, scene.getWidth(), scene.getHeight());
                LOGGER.info("Animation started...");
            }
            a.drawAnimation(s, graphics);
            runTimeSim = nanoTime() - startTimeSim;
        }
    }

    public void restartSim() {
        printRestartingText();
        timeline.stop();
        resetForRestart();
        start();
        tStart = nanoTime();
    }

    public void updateFrameIfPaused() {
        if (timeline.getStatus() == PAUSED) {
            manageSim(canvas.getGraphicsContext2D());
        }
    }

    private void resetForRestart() {
        startTimeSim = 0;
        uc.resetButtonSettings();
        resetColors();
        a.resetAnimationVars();
        reset();
    }

    private void reset() {
        tStart = tStop = 0;
    }

    private void printRestartingText() {
        double stopTimeSim = nanoTime();
        final var restartingText = "restarting animation...%nAnimation Runtime: %.2f seconds%n%s%n"
                .formatted((stopTimeSim - startTimeSim) / 1e9, DOTTED_LINE);
        LOGGER.info(restartingText);
    }

    private void printClosingText() {
        double stopTimeSim = nanoTime();
        final var closingText = "%s%nAnimation Runtime of instance %d: %.2f seconds%n%s%n"
                .formatted(CLOSING_MESSAGE, instance, (stopTimeSim - startTimeSim) / 1e9, DOTTED_LINE);
        LOGGER.info(closingText);
    }
}
