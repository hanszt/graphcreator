package hzt.model.custom_controls;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.Button;

public class SwitchButton extends Button {

    private final String activeLabel;
    private final String inActiveLabel;
    private final MultipleHandlersHandler multipleHandlersHandler;

    private boolean active;
    private boolean switchModeEnabled;
    private String activeStyle;
    private String inActiveStyle;

    public SwitchButton() {
        this(false);
    }

    private SwitchButton(boolean active) {
        this(active, "active", "off");
    }

    public SwitchButton(boolean active, String enabledText, String disabledText) {
        this(active, enabledText, disabledText, null);
    }

    private SwitchButton(boolean active, String enabledText, String disabledText, Node node) {
        super(active ? enabledText : disabledText, node);
        this.active = active;
        this.activeLabel = enabledText;
        this.inActiveLabel = disabledText;
        this.multipleHandlersHandler = new MultipleHandlersHandler();
        this.multipleHandlersHandler.addEventHandler(activateChangeStateAndTextOnClick());
        this.setOnAction(multipleHandlersHandler);
        this.switchModeEnabled = true;
    }

    private EventHandler<ActionEvent> activateChangeStateAndTextOnClick() {
        return e -> {
            if (switchModeEnabled) {
                SwitchButton.this.active = !active;
                SwitchButton.this.setButtonLabel();
                SwitchButton.this.setStyleByStatus();
            }
        };
    }

    private void setButtonLabel() {
        if (active) super.setText(activeLabel);
        else super.setText(inActiveLabel);
    }

    private void setStyleByStatus() {
        if (active) super.setStyle(activeStyle);
        else super.setStyle(inActiveStyle);
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
        setToSwitchModeLabelAndStyle();
    }

    private void setToSwitchModeLabelAndStyle() {
        setButtonLabel();
        setStyleByStatus();
    }

    public void setSwitchModeEnabled(boolean switchModeEnabled) {
        this.switchModeEnabled = switchModeEnabled;
        setToSwitchModeLabelAndStyle();
    }

    public void setButtonFocused(boolean focused) {
        this.setFocused(focused);
    }

    public void setActiveStyle(String activeStyle) {
        this.activeStyle = activeStyle;
    }

    public void setInActiveStyle(String inActiveStyle) {
        this.inActiveStyle = inActiveStyle;
    }

    public MultipleHandlersHandler getMultipleHandlersHandler() {
        return multipleHandlersHandler;
    }
}
