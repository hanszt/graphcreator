package hzt.model;

import javafx.geometry.Point2D;
import javafx.scene.Scene;
import javafx.scene.paint.Color;

import java.util.ArrayList;
import java.util.List;

import static hzt.controller.AppConstants.INIT_SCENE_SIZE;
import static hzt.view.AnimationVars.*;
import static java.lang.Math.*;
import static javafx.scene.paint.Color.*;

public class Flock extends ArrayList<Boid> {

    public static final String CIRCLE = "circle", CIRCLE_UPPER = "circle upper",
            CIRCLE_UPPER_LEFT = "circle upper left", RANDOM = "random", LINEAR = "linear";
    private static final Point2D INIT_START_POINT_CIRCLE = new Point2D(INIT_SCENE_SIZE.getWidth() / 2, INIT_SCENE_SIZE.getHeight() / 2);
    private static final Point2D INIT_START_POINT_CIRCLE_UPPER = new Point2D(INIT_SCENE_SIZE.getWidth() / 2, 0);
    private static final Point2D INIT_START_POINT_CIRCLE_UPPER_LEFT = new Point2D(0, 0);

    private final String type;
    private Point2D startPoint;
    private double radiusPerceptionRadiusRatio;

    public Flock(String type) {
        this(type, new ArrayList<>());
    }

    public Flock(List<Boid> boids) {
        this(RANDOM, boids);
    }

    private Flock(String type, List<Boid> boids) {
        super(boids);
        this.type = type;
        this.radiusPerceptionRadiusRatio = INIT_PERCEPTION_RADIUS_RATIO;
        this.startPoint = type.equals(CIRCLE) ? INIT_START_POINT_CIRCLE : type.equals(CIRCLE_UPPER) ? INIT_START_POINT_CIRCLE_UPPER :
                type.equals(CIRCLE_UPPER_LEFT) ? INIT_START_POINT_CIRCLE_UPPER_LEFT : null;
        fillFlockBasedOnType(INIT_NUMBER_OF_BOIDS, INIT_SPEED_MULTIPLIER, INIT_SCENE_SIZE.getWidth(), INIT_SCENE_SIZE.getHeight());
    }

    private void fillFlockBasedOnType(int numberOfBoids, double speedMultiplier, double width, double height) {
        switch (type) {
            case RANDOM -> setupRandomFlock(width, height, speedMultiplier);
            case LINEAR -> setupLinearFlock(numberOfBoids, speedMultiplier);
            case CIRCLE_UPPER, CIRCLE_UPPER_LEFT, CIRCLE -> setupCircleFlock(startPoint, numberOfBoids, speedMultiplier);
            default -> throw new IllegalStateException(type + " is not a valid type");
        }
    }

    private void setupCircleFlock(Point2D startPoint, int numberOfBoids, double speedMultiplier) {
        for (int i = 0; i < numberOfBoids; i++) {
            Point2D velocityVector = new Point2D(speedMultiplier * cos((2 * i * PI) / numberOfBoids),
                    speedMultiplier * sin((2 * i * PI) / numberOfBoids));
            Point2D accelerationVector = new Point2D(0, 0);
            Color color = i % 6 == 0 ? DARKBLUE : i % 6 == 1 ? ORANGE : i % 6 == 2 ? WHITE : i % 6 == 3 ? DARKGREEN : i % 6 == 4 ? DARKRED : BLACK;
            this.push(new Boid(startPoint, velocityVector, accelerationVector, 3, color));
        }
    }

    private void setupRandomFlock(double width, double height, double speedMultiplier) {
        for (int i = 0; i < INIT_NUMBER_OF_BOIDS; i++) {
            this.push(new Boid(new Point2D(width * Math.random(), height * Math.random()),
                    new Point2D((Math.random() - 0.5) * speedMultiplier, (Math.random() - 0.5) * speedMultiplier)));
        }
    }

    private void setupLinearFlock(int numberOfBoids, double speedMultiplier) {
        for (int i = 0; i < numberOfBoids; i++) {
            Point2D startPoint = i < numberOfBoids / 2 ? new Point2D(i * INIT_SCENE_SIZE.getWidth() * 2 / numberOfBoids, 0) :
                    new Point2D(0, (i - numberOfBoids / 2.) * INIT_SCENE_SIZE.getHeight() * 2 / numberOfBoids);
            Point2D velocityVector = i < numberOfBoids / 2 ? new Point2D(0, speedMultiplier) : new Point2D(speedMultiplier, 0);
            Color color = i % 6 == 0 ? DARKBLUE : i % 6 == 1 ? ORANGE : i % 6 == 2 ? WHITE : i % 6 == 3 ? DARKGREEN : i % 6 == 4 ? DARKRED : BLACK;
            this.push(new Boid(startPoint, velocityVector, Point2D.ZERO, 3, color));
        }
    }

    public int getTotalAmountOfConnections() {
        int totalConnectionAmount = 0;
        for (Boid b : this) totalConnectionAmount += b.getAmountOfConnections();
        return totalConnectionAmount;
    }

    public void push(Boid boid) {
        add(boid);
    }

    public Boid pop() {
        return remove(size() - 1);
    }

    public Boid getBoid(int index) {
        return get(index);
    }

    public void reset(int numberOfBoids, double speedMultiplier, Scene scene) {
        double sceneWidth = scene.getWidth(), sceneHeight = scene.getHeight();
        startPoint = type.equals(CIRCLE) ? new Point2D(sceneWidth / 2, sceneHeight / 2) :
                type.equals(CIRCLE_UPPER) ? new Point2D(sceneWidth / 2, 0) :
                        type.equals(CIRCLE_UPPER_LEFT) ? new Point2D(0, 0) :  null;
        radiusPerceptionRadiusRatio = INIT_PERCEPTION_RADIUS_RATIO;
        clear();
        fillFlockBasedOnType(numberOfBoids, speedMultiplier, sceneWidth, sceneHeight);
    }

    public void setToAmount(int intValue, double speedMultiplier, Scene scene) {
        while (size() > intValue) remove(0);
        int i = 0;
        while (size() < intValue) {
            Point2D velocityVector = new Point2D(speedMultiplier * cos((2 * i * PI) / intValue), speedMultiplier * sin((2 * i * PI) / intValue));
            Point2D accelerationVector = new Point2D(0, 0);
            Color color = i % 6 == 0 ? DARKBLUE : i % 6 == 1 ? ORANGE : i % 6 == 2 ? WHITE : i % 6 == 3 ? DARKGREEN : i % 6 == 4 ? DARKRED : BLACK;
            switch (type) {
                case CIRCLE:
                    this.push(new Boid(new Point2D(scene.getWidth() / 2, scene.getHeight() / 2), velocityVector, accelerationVector, 3, color));
                    break;
                case CIRCLE_UPPER:
                    this.push(new Boid(new Point2D(scene.getWidth() / 2, 0), velocityVector, accelerationVector, 3, color));
                    break;
                case CIRCLE_UPPER_LEFT:
                    this.push(new Boid(new Point2D(0, 0), velocityVector, accelerationVector, 3, color));
                    break;
                default:
                    add(new Boid(
                            new Point2D(Math.random() * scene.getWidth(), Math.random() * scene.getHeight()),
                            new Point2D((Math.random() - 0.5) * speedMultiplier, (Math.random() - 0.5) * speedMultiplier)));
                    break;
            }
            i++;
        }
    }

    public Flock getBoids() {
        return this;
    }

    public String getType() {
        return type;
    }

    public double getRadiusPerceptionRadiusRatio() {
        return radiusPerceptionRadiusRatio;
    }

    public void setRadiusPerceptionRadiusRatio(double radiusPerceptionRadiusRatio) {
        this.radiusPerceptionRadiusRatio = radiusPerceptionRadiusRatio;
    }
}
