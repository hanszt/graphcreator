package hzt.model;

import javafx.geometry.Point2D;
import javafx.scene.Scene;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import hzt.model.custom_controls.SwitchButton;
import hzt.view.ColorController;

import static hzt.controller.AppConstants.INIT_SCENE_SIZE;
import static hzt.view.AnimationVars.INIT_PERCEPTION_RADIUS_RATIO;

public class Boid {

    private static final double RADIUS_LOWER_BOUND = 3, RADIUS_UPPER_BOUND = 10;

    private final Point2D initPosition;
    private final Point2D initVelocityVector;
    private final Point2D initAccelerationVector;

    private Point2D position;
    private Point2D velocityVector;
    private Point2D accelerationVector;
    private final Color color;
    private double radius;
    private double perceptionRadius;
    private int amountOfConnections;

    public Boid() {
        this(new Point2D(INIT_SCENE_SIZE.getWidth() * Math.random(), INIT_SCENE_SIZE.getHeight() * Math.random()));
    }

    public Boid(Point2D position) {
        this(position, new Point2D(Math.random() - 0.5, Math.random() - 0.5));
    }

    public Boid(Point2D position, Point2D velocityVector) {
        this(position, velocityVector, new Point2D(0, 0));
    }

    public Boid(Point2D position, Point2D velocityVector, Point2D accelerationVector) {
        this(position, velocityVector,accelerationVector,
                Math.random() * (RADIUS_UPPER_BOUND - RADIUS_LOWER_BOUND) + RADIUS_LOWER_BOUND,
                Color.color(Math.random(), Math.random(), Math.random()));
    }

    public Boid(Point2D position, Point2D velocityVector, Point2D accelerationVector, double radius, Color color) {
        this.initPosition = position;
        this.initVelocityVector = velocityVector;
        this.initAccelerationVector = accelerationVector;
        this.position = position;
        this.velocityVector = velocityVector;
        this.accelerationVector = accelerationVector;
        this.radius = radius;
        this.color = color;
        this.perceptionRadius = INIT_PERCEPTION_RADIUS_RATIO * radius;
    }

    private void floatThroughEdges(Scene scene) {
        if (position.getX() > scene.getWidth()) {
            position = new Point2D(scene.getWidth() - prevPosition.getX(), position.getY());
        } else if (position.getX() < 0) {
            position = new Point2D(scene.getWidth() - prevPosition.getX(), position.getY());
        }
        if (position.getY() > scene.getHeight()) {
            position = new Point2D(position.getX(), scene.getHeight() - prevPosition.getY());
        } else if (position.getY() < 0) {
            position = new Point2D(position.getX(), scene.getHeight() - prevPosition.getY());
        }
    }

    private Point2D prevPosition;

    private void bounceOfEdges(Scene scene) {
            if (position.getX() > scene.getWidth() && position.getX() > prevPosition.getX())
                velocityVector = new Point2D(-velocityVector.getX(), velocityVector.getY());
            if (position.getX() < 0 && position.getX() < prevPosition.getX())
                velocityVector = new Point2D(-velocityVector.getX(), velocityVector.getY());
            if (position.getY() > scene.getHeight() && position.getY() > prevPosition.getY())
                velocityVector = new Point2D(velocityVector.getX(), -velocityVector.getY());
            if (position.getY() < 0 && position.getY() < prevPosition.getY())
                velocityVector = new Point2D(velocityVector.getX(), -velocityVector.getY());
    }

    private static final int VELOCITY_INIT_VEL_RATIO = 100;

    public void updatePosition(Flock flock, double deltaTime, Scene scene, SwitchButton bounceButton) {
//        accelerationVector = gravity(flock);
        velocityVector = velocityVector.add(accelerationVector.multiply(deltaTime));
//        velocityVector = velocityVector.add(accelerationVector.multiply(deltaTime));

//        if (velocityVector.magnitude() > initVelocityVector.magnitude() * VELOCITY_INIT_VEL_RATIO) {
//            velocityVector = initVelocityVector.multiply(VELOCITY_INIT_VEL_RATIO);
//        }
        prevPosition = position;
        position = position.add(velocityVector.multiply(deltaTime));
//        steeringForce(flock, 50);
        if (bounceButton.isActive()) bounceOfEdges(scene);
        else floatThroughEdges(scene);
    }

    //TODO: Make this work
    public Point2D adjustToNeighborSpeedAndDir(Flock flock, int perceptionRadius) {
        int totalInRadius = 0;
        Point2D avgVelocityVector = new Point2D(0, 0);
        for (Boid other : flock.getBoids()) {
            if (!equals(other) && position.distance(other.position) < perceptionRadius) {
                avgVelocityVector = avgVelocityVector.add(other.velocityVector);
                totalInRadius++;
            }
        }
        avgVelocityVector = new Point2D(avgVelocityVector.getX() / totalInRadius, avgVelocityVector.getY() / totalInRadius);
//        velocityVector = avgVelocityVector;
        return velocityVector.subtract(avgVelocityVector);
    }

    //TODO: make this work
    private static final double GRAVITY_CONSTANT = 9.8;

    public Point2D gravity(Flock flock) {
        Point2D totalGravityAccVector = new Point2D(0, 0);
        for (Boid other : flock.getBoids()) {
            double distance = this.position.distance(other.position);
            Point2D thisGravityAccVector = new Point2D(this.velocityVector.getY(), this.velocityVector.getX()).normalize().multiply(distance);
            totalGravityAccVector = totalGravityAccVector.add(thisGravityAccVector);
        }
        return totalGravityAccVector.multiply(0.0001);
    }

    public void drawBoid(GraphicsContext gc, SwitchButton uniformButton) {
        if(uniformButton.isActive()) gc.setFill(color);
        else gc.setFill(ColorController.getUniformColor());
        gc.fillOval(position.getX() - radius, position.getY() - radius, radius * 2, radius * 2);
    }

    private static final int MAX_CONNECTIONS_PER_BOID = 100;

    public void drawConnectionsInPerceptionRadius(Flock flock, SwitchButton fadeOutButton, SwitchButton uniformButton, GraphicsContext gc) {
        perceptionRadius = flock.getRadiusPerceptionRadiusRatio() * radius;
        amountOfConnections = 0;
        for (Boid other : flock.getBoids()) {
            if (!this.equals(other) && position.distance(other.position) <= perceptionRadius && amountOfConnections < MAX_CONNECTIONS_PER_BOID) {
                gc.setLineWidth(radius / 4);
                setColor(uniformButton, fadeOutButton, gc, other);
                gc.strokeLine(position.getX(), position.getY(), other.getPosition().getX(), other.getPosition().getY());
                amountOfConnections++;
            }
        }
    }

    private void setColor(SwitchButton uniformButton, SwitchButton connectionFadeOutButton, GraphicsContext gc, Boid other) {
        if (connectionFadeOutButton.isActive()) {
            if (uniformButton.isActive()) gc.setStroke(color);
            else gc.setStroke(ColorController.getUniformColor());
        }
        else {
            double opacity = (-1 / perceptionRadius) * position.distance(other.position) + 1;
            if (uniformButton.isActive()) {
                gc.setStroke(Color.hsb(color.getHue(), color.getSaturation(), color.getBrightness(), opacity));
            } else {
                Color color = ColorController.getUniformColor();
                gc.setStroke(Color.hsb(color.getHue(), color.getSaturation(), color.getBrightness(), opacity));
            }
        }
    }


    public void scaleSpeed(double scale) {
        velocityVector = velocityVector.multiply(scale);
    }

    public void scaleAcceleration(double scale) {
        accelerationVector = accelerationVector.multiply(scale);
    }

    public void scalePerceptionRadius(double multiplier) {
        perceptionRadius = radius * multiplier;
    }

    public Point2D getPosition() {
        return position;
    }

    public double getRadius() {
        return radius;
    }

    public double getPerceptionRadius() {
        return perceptionRadius;
    }

    public void setPosition(Point2D position) {
        this.position = position;
    }

    public Color getColor() {
        return color;
    }

    public int getAmountOfConnections() {
        return amountOfConnections;
    }

    @Override
    public String toString() {
        return "Boid{" +
                "position=" + position +
                ", velocityVector=" + velocityVector +
                ", accelerationVector=" + accelerationVector +
                ", color=" + color +
                ", radius=" + radius +
                ", perceptionRadius=" + perceptionRadius +
                '}';
    }
}
