package hzt.view;

import hzt.controller.AppManager;

abstract class UpdateVars extends AnimationVars {

    UpdateVars(AppManager m, UserInputController uc) {
        super(m, uc);
    }

    private void updateTime() {
        time += getDeltaTime();
    }

    double getDeltaTime() {
        return  m.getTimeline().getRate() >= 0 ? .001 : -.001;
    }
}
