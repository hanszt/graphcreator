package hzt.view;

public interface Updater {

    void update(boolean condition);
}
