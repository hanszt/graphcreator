package hzt.view;

import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;

import static java.lang.Math.random;
import static javafx.scene.paint.Color.*;

public class ColorController {

    static Color backgroundColor = SKYBLUE;
    static Color uniformColor = SADDLEBROWN;
    static Color statsColor = WHITE;
    static Color textColor = DARKBLUE;

    private static Color getRandomColor() {
        Color color;
        int selector = (int) (random() * 10);
        if (selector == 0) color = PINK;
        else if (selector == 1) color = ORANGE;
        else if (selector == 2) color = YELLOW;
        else if (selector == 3) color = LIGHTGREEN;
        else if (selector == 4) color = LIGHTBLUE;
        else if (selector == 6) color = PURPLE;
        else if (selector == 7) color = RED;
        else if (selector == 8) color = MAROON;
        else if (selector == 9) color = GREEN;
        else color = BLUE;
        return color;
    }

    public static void resetColors() {
        backgroundColor = SKYBLUE;
        uniformColor = SADDLEBROWN;
        textColor = DARKBLUE;
        statsColor = WHITE;
    }

    public static Color getBackgroundColor() {
        return backgroundColor;
    }

    public static Color getUniformColor() {
        return uniformColor;
    }

    public static Paint getTextColor() {
        return textColor;
    }
}

