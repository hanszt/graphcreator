package hzt.view;

import hzt.controller.AppManager;
import javafx.geometry.VPos;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;
import hzt.model.Flock;

import static hzt.view.ColorController.*;

public class StatsController {

    private final AppManager m;
    private final DrawAnimation a;

    public StatsController(AppManager m, DrawAnimation a) {
        this.m = m;
        this.a = a;
    }

    private static final int FONT_SIZE_STATS = 15;

    void getStats(Flock flock, GraphicsContext graphics) {
        drawVarNames(graphics);
        final int numberPosX = 140;
        graphics.setFill(textColor);
        graphics.fillText(String.format("\n\n%d\n%d\n\n\n%.2f\n%d f/s\n%.2f s",
                flock.getBoids().size(), flock.getTotalAmountOfConnections(), a.curFlock.getRadiusPerceptionRadiusRatio(),
                getFrameRate(globalFrameCountNew), m.getRunTimeSim() / 1e9), numberPosX, 0);
    }

    private void drawVarNames(GraphicsContext graphics) {
        graphics.setTextBaseline(VPos.TOP);
        graphics.setTextAlign(TextAlignment.LEFT);
        graphics.setFont(Font.font("", FONT_SIZE_STATS));
        graphics.setFill(statsColor);
        graphics.fillText("\nStats:\n# of boids:\n# of connections:\nConnection\nToBallRadius\nRatio: \nFrameRate:\nRuntime: ", 0, 0);
    }

    private long t1 = 0;
    private int frameCountOld;
    private int globalFrameCountNew = 0, frameRate = 0;

    private int getFrameRate(int frameCountNew) {
        globalFrameCountNew++;
        if (globalFrameCountNew > 1e4) globalFrameCountNew = 0;
        if (t1 == 0) t1 = System.nanoTime();
        long t2 = System.nanoTime();
        if (t2 - t1 > 1e9) {
            long deltaT = t2 - t1;
            t1 = 0;
            int deltaFrameCount = frameCountNew - frameCountOld;
            int newFramerate = (int) (1e9 * deltaFrameCount / deltaT);
            frameRate = newFramerate < 0 ? frameRate : newFramerate;
            frameCountOld = frameCountNew;
        }
        return frameRate;
    }
}
