package hzt.view;

import hzt.controller.AppManager;
import hzt.model.Boid;
import hzt.model.Flock;
import hzt.model.custom_controls.SwitchButton;
import javafx.geometry.Insets;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.util.StringConverter;

import java.util.HashSet;
import java.util.Set;

import static hzt.controller.AppConstants.SCREEN_SIZE;
import static hzt.view.AnimationVars.INIT_PERCEPTION_RADIUS_RATIO;
import static hzt.view.ColorController.*;
import static javafx.animation.Animation.Status.PAUSED;

public class UserInputController {

    private static final int DIST_FROM_EDGE = 20;
    private static final int PREF_WIDTH_MENU = 200;
    private static final int INIT_V_POS_CONTROLS = 0;
    private static final int PREF_HEIGHT_BUTTONS = 40;
    private static final double OPACITY_VALUE = 0.7;

    private final AppManager m;
    private DrawAnimation a;


    public UserInputController(AppManager m) {
        this.m = m;
    }

    private int vPosControls = INIT_V_POS_CONTROLS;
    private double controlsOpacity = OPACITY_VALUE;

    public GridPane setupControls(DrawAnimation a) {
        this.a = a;
        GridPane controlsGrid = setupGridPane();
        setupLabel(controlsGrid);
        setupInputTypeCombobox(controlsGrid);
        setupButtons(controlsGrid);
        setupColorPickers(controlsGrid);
        setupSliders(controlsGrid);
        for (Node n : controlsGrid.getChildren()) setGlobalParams(n);
        return controlsGrid;
    }

    private void setupLabel(GridPane grid) {
        Label label = new Label();
        label.setPrefHeight(SCREEN_SIZE.height * 9. / 12);
        grid.add(label, 0, vPosControls++, 2, 1);
    }

    private void setGlobalParams(Node n) {
//        n.setCursor(Cursor.HAND);
        n.setOpacity(controlsOpacity);
    }

    private GridPane setupGridPane() {
        GridPane grid = new GridPane();
        grid.setHgap(2);
        grid.setVgap(4);
//        grid.setAlignment(Pos.BOTTOM_LEFT);
//        double yScaleFactor = (double) m.getSceneSize().height / SCREEN_SIZE.height;
//        if (yScaleFactor > 1) yScaleFactor = 1;
//        grid.setScaleY(yScaleFactor);
        grid.setPadding(new Insets(DIST_FROM_EDGE, DIST_FROM_EDGE, DIST_FROM_EDGE * 2, DIST_FROM_EDGE));
        return grid;
    }

    private void setupInputTypeCombobox(GridPane grid) {
        final ComboBox<Flock> options = setupComboBox(grid, vPosControls++);
        Set<Flock> flockSet = getFlockSet();
        for (Flock c : flockSet) options.getItems().add(c);
        options.setValue(a.curFlock);
        options.setOnAction(e -> convertWithSelected(options.getValue()));
    }

    private Set<Flock> getFlockSet() {
        a.flockSet = new HashSet<>();
        a.flockSet.add(a.curFlock);
        a.flockSet.add(new Flock(Flock.RANDOM));
        a.flockSet.add(new Flock(Flock.CIRCLE_UPPER));
        a.flockSet.add(new Flock(Flock.CIRCLE_UPPER_LEFT));
        a.flockSet.add(new Flock(Flock.LINEAR));
        return a.flockSet;
    }

    SliderUpdater sliderUpdater;

    private void convertWithSelected(Flock selected) {
        a.curFlock = selected;
        m.updateFrameIfPaused();
        sliderUpdater.updateSliders(selected.size(), selected.getRadiusPerceptionRadiusRatio(), 0);
    }

    private <T> ComboBox<T> setupComboBox(GridPane grid, int vPos) {
        ComboBox<T> comboBox = new ComboBox<>();
        comboBox.setPrefWidth(PREF_WIDTH_MENU);
        comboBox.minWidth(PREF_WIDTH_MENU);
        comboBox.setConverter(getStringConverter());
        grid.add(comboBox, 0, vPos, 2, 1);
        return comboBox;
    }

    private <T> StringConverter<T> getStringConverter() {

        return new StringConverter<T>() {
            @Override
            public String toString(T item) {
                if (item instanceof Flock) return ((Flock) item).getType();
                else return null;
            }

            @Override
            public T fromString(String id) {
                return null;
            }
        };
    }

    SwitchButton showStatsButton;
    private SwitchButton pauseButton;
    SwitchButton bounceButton;
    private SwitchButton visibleControlsButton;
    SwitchButton connectionFadeOutButton;
    SwitchButton multiColorButton;
    SwitchButton showBoidsButton;

    private void setupButtons(GridPane grid) {
        Button restart = setupButton(grid, "restart sim", 0, vPosControls);
        Button newInstance = setupButton(grid, "start new instance", 1, vPosControls++);
        multiColorButton = setupSwitchButton(grid, true, "uniform color", "different colors", 0, vPosControls);
        showStatsButton = setupSwitchButton(grid, false, "hide stats", "show stats", 1, vPosControls++);
        pauseButton = setupSwitchButton(grid, false, "play sim", "pause sim", 0, vPosControls);
        bounceButton = setupSwitchButton(grid, true, "float", "bounce", 1, vPosControls++);
        visibleControlsButton = setupSwitchButton(grid, true, "transparent", "solid", 0, vPosControls);
        connectionFadeOutButton = setupSwitchButton(grid, false, "fade out", "step wise", 1, vPosControls++);
        showBoidsButton = setupSwitchButton(grid, true, "hide boids", "show boids", 0, vPosControls);
        Button resetCurFlockButton = setupButton(grid, "reset flock", 1, vPosControls++);
        pauseButton.getMultipleHandlersHandler().addEventHandler(0, e -> {
            if (pauseButton.isActive()) {
                m.getTimeline().play();
                bounceButton.setSwitchModeEnabled(true);
            } else {
                m.getTimeline().pause();
                bounceButton.setSwitchModeEnabled(false);
                bounceButton.setText("update frame");
            }
            pauseButton.setButtonFocused(m.getTimeline().getStatus().equals(PAUSED));
        });
        restart.setOnAction(e -> m.restartSim());
        resetCurFlockButton.setOnAction(e -> {
            sliderUpdater.updateSliders(a.curFlock.size(), INIT_PERCEPTION_RADIUS_RATIO, 0);
            a.curFlock.reset(a.curFlock.size(), a.speedMultiplier, m.getScene());
        });
        bounceButton.getMultipleHandlersHandler().addEventHandler(e -> {
            if (pauseButton.isActive()) m.updateFrameIfPaused();
        });
        visibleControlsButton.getMultipleHandlersHandler().addEventHandler(e -> {
            controlsOpacity = visibleControlsButton.isActive() ? OPACITY_VALUE : 0;
            for (Node c : grid.getChildren()) c.setOpacity(controlsOpacity);
        });
        newInstance.setOnAction(e -> new AppManager().setupStage(new Stage()));
        visibleControlsButton.setCursor(Cursor.HAND);
    }

    private Button setupButton(GridPane grid, String text, int hPos, int vPos) {
        Button button = new Button(text);
        button.setPrefWidth(PREF_WIDTH_MENU / 2.);
        grid.add(button, hPos, vPos);
        return button;
    }

    private SwitchButton setupSwitchButton(GridPane grid, boolean on, String onText, String offText, int hPos, int vPos) {
        SwitchButton switchButton = new SwitchButton(on, onText, offText);
        switchButton.setPrefWidth(PREF_WIDTH_MENU / 2.);
        switchButton.setMinWidth(PREF_WIDTH_MENU / 2.);
        switchButton.setPrefHeight(PREF_HEIGHT_BUTTONS);
        switchButton.getMultipleHandlersHandler().addEventHandler(e -> m.updateFrameIfPaused());
        grid.add(switchButton, hPos, vPos);
        return switchButton;
    }

    public void resetButtonSettings() {
    }

    private void setupColorPickers(GridPane grid) {
        ColorPicker circleColorPicker = setupColorPicker(uniformColor);
        ColorPicker backgroundColorPicker = setupColorPicker(backgroundColor);
        ColorPicker specificPathColorPicker = setupColorPicker(statsColor);
        ColorPicker finalPathColorPicker = setupColorPicker(textColor);
        specificPathColorPicker.setOnAction(e -> statsColor = specificPathColorPicker.getValue());
        finalPathColorPicker.setOnAction(e -> textColor = finalPathColorPicker.getValue());
        circleColorPicker.setOnAction(e -> uniformColor = circleColorPicker.getValue());
        backgroundColorPicker.setOnAction(e -> {
            Color bgColor = backgroundColorPicker.getValue();
            backgroundColor = backgroundColorPicker.getValue();
            m.getRoot().setBackground(new Background(new BackgroundFill(bgColor, CornerRadii.EMPTY, Insets.EMPTY)));
        });
        grid.add(specificPathColorPicker, 0, vPosControls);
        grid.add(finalPathColorPicker, 1, vPosControls++);
        grid.add(circleColorPicker, 0, vPosControls);
        grid.add(backgroundColorPicker, 1, vPosControls++);
    }

    private ColorPicker setupColorPicker(Color startColor) {
        ColorPicker colorPicker = new ColorPicker(startColor);
        colorPicker.setPrefWidth(PREF_WIDTH_MENU / 2.);
        colorPicker.setStyle("-fx-color-label-visible: false ;");
        return colorPicker;
    }

    private void setupSliders(GridPane grid) {
        vPosControls = vPosControls - 2;
//        Slider flockAcc = setupHorSlider(grid, -10, 10, 1, 2, vPosControls++, 4, true);
        Slider flockSpeedSlider = setupHorSlider(grid, -100, 100, a.speedMultiplier, 2, vPosControls++, 4, true);
        Slider perceptionRadiusSlider = setupHorSlider(grid, 0, 100, a.curFlock.getRadiusPerceptionRadiusRatio(), 2, vPosControls, 2, false);
        Slider amountOfBoidsSlider = setupHorSlider(grid, 1, 1000, a.curFlock.size(), 4, vPosControls, 2, true);
        perceptionRadiusSlider.valueProperty().addListener((ov, oldVal, newVal) -> {
            a.curFlock.setRadiusPerceptionRadiusRatio(newVal.doubleValue());
            m.updateFrameIfPaused();
        });
//        flockAcc.valueProperty().addListener((oldVal, curVal, newVal) -> {
//            for (Boid b : a.randomFlock.getBoids()) b.scaleAcceleration(newVal.doubleValue() / curVal.doubleValue());
//        });
//        flockSpeedSlider.onMousePressedProperty().set(e ->  m.getTimeline().pause());
//        flockSpeedSlider.onMouseReleasedProperty().set(e ->  m.getTimeline().play());
        flockSpeedSlider.valueProperty().addListener((oldVal, curVal, newVal) -> {
            a.speedMultiplier = flockSpeedSlider.getValue();
            for (Boid b : a.curFlock.getBoids()) b.scaleSpeed(newVal.doubleValue() / curVal.doubleValue());
        });
        amountOfBoidsSlider.valueProperty().addListener((oldVal, curVal, newVal) -> {
            a.curFlock.setToAmount(newVal.intValue(), a.speedMultiplier, m.getScene());
            m.updateFrameIfPaused();
        });
        amountOfBoidsSlider.setPrefHeight(10);
        grid.add(new Label(), 2, vPosControls);
        sliderUpdater = (v1, v2, v3) -> {
            amountOfBoidsSlider.setValue(v1);
            perceptionRadiusSlider.setValue(v2);
        };
    }

    private Slider setupHorSlider(GridPane grid, double minValue, double maxValue, double curValue, int hPos, int vPos, int hSpan, boolean hasScale) {
        Slider slider = new Slider(minValue, maxValue, curValue);
        slider.setPrefWidth((m.getScene().getWidth() - DIST_FROM_EDGE * 2));
        slider.setPrefWidth(SCREEN_SIZE.getWidth());
        slider.setPrefHeight(PREF_HEIGHT_BUTTONS);
        if (hasScale) {
            slider.setShowTickLabels(true);
            slider.setShowTickMarks(true);
            slider.setMajorTickUnit(50);
            slider.setMinorTickCount(5);
            slider.setBlockIncrement(10);
        }
        grid.add(slider, hPos, vPos, hSpan, 1);
        return slider;
    }
}
