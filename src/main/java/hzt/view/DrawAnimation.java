package hzt.view;

import hzt.controller.AppManager;
import javafx.animation.Animation;
import javafx.scene.canvas.GraphicsContext;
import hzt.model.Boid;
import hzt.model.Flock;

public class DrawAnimation extends UpdateVars {

    public DrawAnimation(AppManager m, UserInputController uc) {
        super(m, uc);
    }

    public void drawAnimation(StatsController s, GraphicsContext graphics) {
        graphics.clearRect(0, 0, m.getScene().getWidth(), m.getScene().getHeight());
        drawBoids(curFlock, graphics);
        if (uc.showStatsButton.isActive()) s.getStats(curFlock, graphics);
    }

    private void drawBoids(Flock flock, GraphicsContext gc) {
        for (Boid b : flock.getBoids()) {
            if (m.getTimeline().getStatus().equals(Animation.Status.RUNNING)) {
                b.updatePosition(flock, 1, m.getScene(), uc.bounceButton);
            }
            if(uc.showBoidsButton.isActive()) b.drawBoid(gc, uc.multiColorButton);
            b.drawConnectionsInPerceptionRadius(flock, uc.connectionFadeOutButton, uc.multiColorButton, gc);
        }
    }
}
