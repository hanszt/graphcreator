package hzt.view;

@FunctionalInterface
public interface SliderUpdater {

    void updateSliders(double v1, double v2, double v3);
}
