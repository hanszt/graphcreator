package hzt.view;

import hzt.controller.AppManager;
import hzt.model.Flock;

import java.util.Set;

public abstract class AnimationVars {

    final AppManager m;
    final UserInputController uc;

    AnimationVars(AppManager m, UserInputController uc) {
        this.m = m;
        this.uc = uc;
    }

    // animation constants
    public static final int INIT_PERCEPTION_RADIUS_RATIO = 10;
    public static final int INIT_NUMBER_OF_BOIDS = 420;
    public static final int INIT_SPEED_MULTIPLIER = 1;

    double time;
    double speedMultiplier = INIT_SPEED_MULTIPLIER;
    Set<Flock> flockSet;
    Flock curFlock = new Flock(Flock.CIRCLE);

    public void resetAnimationVars() {
        speedMultiplier = INIT_SPEED_MULTIPLIER;
        for (Flock f : flockSet) {
            f.reset(curFlock.size(), speedMultiplier, m.getScene());
        }
        uc.sliderUpdater.updateSliders(curFlock.getRadiusPerceptionRadiusRatio(), curFlock.size(), 0);
    }
}


