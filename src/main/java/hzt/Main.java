package hzt;

import hzt.controller.AppManager;
import javafx.application.Application;
import javafx.stage.Stage;

public class Main extends Application {

    public static void main(String[] args) {
        launch();
    }


    @Override
    public void start(Stage stage) {
        new AppManager().setupStage(stage);
    }
}
