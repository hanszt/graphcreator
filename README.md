# Graph visualizer
Made By Hans Zuidervaart.

## About
This is a program which visualizes graph patterns.

I started this project to make a flocking simulation. 
But at a certain point, I started getting these really cool dynamic graph patterns. So I decided to change the project to that.

Boids or Nodes are connected to each other based on there proximity. The closer they are to each other the stronger the connection becomes.
The boids float over the screen at constant speed as time progresses. 

screenshot 1  Graph with circular start pattern at the middle of the screen
![Circular start pattern](src/main/resources/images/screenshot1.png)


screenshot 2: Graph with circular start pattern at the top of the screen
![Circular start pattern2](src/main/resources/images/screenshot2.png)

screenshot 3: Randomly generated graph
![Random start pattern](src/main/resources/images/screenshot3.png)


## Motivation
To get more insight in graph structures and to get a cool looking animation

## Setup
Before you run the program, make sure you modify the following in Intellij:
- Go to File -->  Settings --> Appearance and Behaviour --> Path Variables
- Add a new path variable by clicking on the + sign and name it PATH_TO_FX and browse to the lib folder of the JavaFX SDK to set its value, and click apply. 
- Go to Run --> Edit Configurations
- Type the following in the VM Options section: --module-path ${PATH_TO_FX} --add-modules javafx.controls,javafx.fxml
- Click Apply and Run the application. It should work now.
    
source: [Getting Started with JavaFX](https://openjfx.io/openjfx-docs/#IDE-Intellij)


### Sources
[Coding Challenge #124: Flocking Simulation](https://www.youtube.com/watch?v=mhjuuHl6qHM)
